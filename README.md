Dentro de los repositorios de las distribuciones podemos encontrar el paquete de la aplicación listo para ser instalado, solo que con el detalle de que posiblemente no sea la versión más actual y además de que esta es solamente una versión de prueba.
Dicho esto, si quieres probar la aplicación te dejo los comandos de instalación.

**Para poder instalar Ardour en Debian, Ubuntu y derivados:**
  
  `sudo apt install ardour`

**Si estas utilizando Arch Linux o algún derivado puedes instalar la aplicación con este comando:**

  `sudo pacman -S ardour`

**Para el caso de Fedora, CentOS y derivados podemos instalar con:**
	
  `sudo dnf install ardour`

**Para el caso openSUSE:**
  
  `sudo zypper install ardour`

Y ya con esto tendrás instalada la aplicación en tu sistema.

para mas información seguir la guía de instalación con el video de drive 

https://drive.google.com/file/d/1VbTmFzUJJEclRyucCCdHETVKXqOrUCzK/view?usp=sharing
